#!/usr/bin/env python
from collections import OrderedDict
from pickle import load, dump
from sklearn import lda, svm

from pylabrecorder import *

from extract_data import ExtractData
from time_helper import *
from argument_helper import parse_arguments
from helper_methods import SiteInfo
from numpy.random import choice as resample

from numpy.random import random_integers


websites = list()
parse_arguments(globals(), sys.argv[1:])

extractData = ExtractData(websites=websites) if websites else ExtractData()
n_users = len(extractData.get_usertokens())



site_identifier = ("_".join([str(site) for site in websites]) if websites else 'all')
sitename, siteinfo = None, None
if len(websites) == 1:
    siteinfo = SiteInfo(websites[0])
    sitename = siteinfo.name
    site_identifier = sitename.replace(' ', '-')

def normalize(v):
    v = v - average(v)
    sigma=sqrt(var(v))
    if sigma==0:
        return v
    return v/sigma

class ArrayData(object):
    def __init__(self, data):
        raw_arrays = OrderedDict()
        arrays = OrderedDict()
        for row in data:
            for key, value in row.items():
                l = raw_arrays.get(key, list())
                l.append(value)
                raw_arrays[key] = l
        for key, value in raw_arrays.items():
            raw_arrays[key] = array(value)
        for key, value in raw_arrays.items():
            if value.dtype.kind in "bif": # if it is a bool, integer or float
                arrays[key] = array(value, float)
            else: # most likely in case it is a string
                classes = set(value)
                for classname in set(value):
                    classname_ = classname.replace(" ", "_") if classname else "NA"
                    arrays[key+"_"+classname_] = array(value == classname, float)
        self.arrays = arrays

    def svm_X(self):
        keys, values = list(), list()
        for key, value in self.arrays.items():
            if key == 'referer_known':
                continue
            if 'referer_uri_netloc' in key:
                continue
            if key == 'control':
                continue
            keys.append(key)
            values.append(array(normalize(value), float64))
            keys.append(key)
        return keys, concatenate([reshape(value, (len(value), 1)) for value in values], 1)

    def svm_Y(self):
        return "control", array(self.arrays["control"], int)

filename = 'svm_pkl/svm_{}.pkl'.format(site_identifier)
try:
    array_data = load(open(filename))
except IOError:
    data = list()
    try:
        for i, user in enumerate(extractData.get_users(session_split_criterium=None)):
            print "****Loading, {:.2%} of total done...".format(i*1.0/n_users)
            assert len(user.sessions) == 1
            session = user.sessions[0]
            row = OrderedDict()

            # row["referer_uri_path"]      = session.referer_uri_path
            # row["user_agent_version"]    = session.user_agent_version
            # row["device_os_version"]     = session.device_os_version


            row["any_conversions"]       = session.any_conversions
            row["any_messageTrackings"]  = session.any_messageTrackings
            row["any_otherMessages"]     = session.any_otherMessages
            row["any_scarcityMessages"]  = session.any_scarcityMessages
            row["any_socialMessages"]    = session.any_socialMessages
            row["any_messages"]          = session.any_socialMessages or session.any_scarcityMessages \
                                                                      or session.any_otherMessages

            row["n_afternoon_pageviews"] = session.n_afternoon_pageviews
            row["n_conversions"]         = session.n_conversions
            row["n_evening_pageviews"]   = session.n_evening_pageviews
            row["n_events"]              = session.n_events
            row["n_messageTrackings"]    = session.n_messageTrackings
            row["n_morning_pageviews"]   = session.n_morning_pageviews
            row["n_night_pageviews"]     = session.n_night_pageviews
            row["n_otherMessages"]       = session.n_otherMessages
            row["n_scarcityMessages"]    = session.n_scarcityMessages
            row["n_socialMessages"]      = session.n_socialMessages
            row["n_trackings"]           = session.n_trackings

            row["avg_conversions"]       = session.n_conversions       / session.n_events
            row["avg_evening_pageviews"] = session.n_evening_pageviews / session.n_events
            row["avg_messageTrackings"]  = session.n_messageTrackings  / session.n_events
            row["avg_morning_pageviews"] = session.n_morning_pageviews / session.n_events
            row["avg_night_pageviews"]   = session.n_night_pageviews   / session.n_events
            row["avg_otherMessages"]     = session.n_otherMessages     / session.n_events
            row["avg_scarcityMessages"]  = session.n_scarcityMessages  / session.n_events
            row["avg_socialMessages"]    = session.n_socialMessages    / session.n_events
            row["avg_trackings"]         = session.n_trackings         / session.n_events
            row["avg_total_spent"]       = session.total_spent         / session.n_events

            row["referer_known"]         = session.referer_known
            row["referer_medium"]        = session.referer_medium
            row["referer_uri_netloc"]    = session.referer_uri_netloc
            row["converted"]             = session.converted
            row["device_os"]             = session.device_os
            row["user_agent"]            = session.user_agent
            row["control"]               = session.control

            row["time_period_s"]         = es_utctime_str_to_epochfloat(session.end_time) - es_utctime_str_to_epochfloat(session.start_time)
            row["total_spent"]           = session.total_spent

            data.append(row)
    except KeyboardInterrupt:
        pass

    array_data = ArrayData(data)
    dump(array_data, open(filename, 'wa'))

X_columns, X = array_data.svm_X()
y_column,  y = array_data.svm_Y()

nsamples = len(y)
ntrain = (2*nsamples)/3

X_train, X_test = X[:ntrain], X[ntrain:]
y_train, y_test = y[:ntrain], y[ntrain:]

model = lda.LDA()
model = model.fit(X_train, y_train)

X_test_tr = model.transform(X_test)
X_test_tr_control = X_test_tr[y_test==1]
X_test_tr_convers = X_test_tr[y_test==0]

f = figure()
ax = f.add_subplot(111)
ax.plot([i[0] for i in X_test_tr_control], [i[1] for i in X_test_tr_control], 'or', label="control")
ax.plot([i[0] for i in X_test_tr_convers], [i[1] for i in X_test_tr_convers], 'og', label="conversified")
ax.legend()
show()

# clf = svm.SVC(probability=True, kernel="linear", class_weight={c:1.0/(sum(y_train==c)) for c in (0, 1)})
# clf.fit(X_train, y_train)
# y_predicted = clf.predict(X_test)


from IPython import embed
embed()

