from time_helper import es_utctime_str_to_epochfloat

import sys
import httpagentparser

from referer_parser import Referer
from numpy import *

from shared.es.model.conversiondocument import ConversionDocument
from shared.es.model.trackingdocument import TrackingDocument
from shared.es.model.messagetrackingdocument import MessageTrackingDocument
from shared.enum.messagetype import MessageType

from time_helper import epochfloat_to_time_of_day, es_utctime_str_to_epochfloat
from easy_str import EasyStr

class Aggregator(object):
    """
    Decorator that adds aggregator functions to functions

    The function itself is replaced by a callable object which behaves as the original function.
    In addition the object contains aggregator methods which let the original function act on the individual elements
    of a list and return an aggregated value.
    """
    registry = dict()

    def __init__(self, function):
        self._function = function
        Aggregator.registry[function.__name__] = self


    def count(self, iterable):
        count = 0
        for i in iterable:
            if self._function(i):
                count += 1
        return count

    def all(self, iterable):
        all = True
        for i in iterable:
            if not self._function(i):
                all = False
        return all

    def any(self, iterable):
        any = False
        for i in iterable:
            if self._function(i):
                any = True
        return any

    def sum(self, iterable):
        sum = 0.0
        for i in iterable:
            if self._function(i):
                sum += self._function(i)
        return sum

    def last(self, iterable):
        result = None
        for i in iterable:
            result = self._function(i)
        return result

    def last_not_none(self, iterable):
        result = None
        for i in iterable:
            value = self._function(i)
            if value != None:
                result = value
        return result

    def __call__(self, x):
        return self._function(x)


@Aggregator
def is_event(event):
    # dummy to count all events, should always return True for now
    # can perhaps be used for filtering some stuff in the future
    return True

@Aggregator
def is_conversion(event):
    return isinstance(event, ConversionDocument)

@Aggregator
def amount_spent(event):
    if isinstance(event, ConversionDocument):
        return event.value
    return None

@Aggregator
def is_tracking(event):
    return isinstance(event, TrackingDocument)

@Aggregator
def is_messageTracking(event):
    return isinstance(event, MessageTrackingDocument)

@Aggregator
def is_socialMessage(event):
    if isinstance(event, MessageTrackingDocument):
        msgt = event.message['type']
        if (msgt == MessageType.CURRENT_VISITORS_SITE or
            msgt == MessageType.CURRENT_VISITORS_PAGE or
            msgt == MessageType.PRODUCT_VIEWS         or
            msgt == MessageType.PRODUCT_BUYS          or
            msgt == MessageType.STORE_REVIEWS         or
            msgt == MessageType.LAST_ORDERED):
            return True
    return False

@Aggregator
def is_scarcityMessage(event):
    if isinstance(event, MessageTrackingDocument):
        msgt = event.message['type']
        if (msgt == MessageType.DELIVERY or
            msgt == MessageType.STOCK):
            return True
    return False


@Aggregator
def is_otherMessage(event):
    if isinstance(event, MessageTrackingDocument):
        msgt = event.message['type']
        if msgt == MessageType.STATIC:
            return True
    return False

@Aggregator
def created_atTime(event):
    return event.created_at

@Aggregator
def messageTime(event):
    if is_messageTracking(event):
        return event.created_at

@Aggregator
def socialMessageTime(event):
    if is_socialMessage(event):
        return event.created_at

@Aggregator
def scarcityMessageTime(event):
    if is_scarcityMessage(event):
        return event.created_at

@Aggregator
def otherMessageTime(event):
    if is_otherMessage(event):
        return event.created_at



@Aggregator
def is_morning_pageview(event):
    if isinstance(event, TrackingDocument):
        time_of_day = epochfloat_to_time_of_day(es_utctime_str_to_epochfloat(event.created_at))
        if time_of_day >= 6.0 and time_of_day < 12.0:
            return True
    return False

@Aggregator
def is_afternoon_pageview(event):
    if isinstance(event, TrackingDocument):
        time_of_day = epochfloat_to_time_of_day(es_utctime_str_to_epochfloat(event.created_at))
        if time_of_day >= 12.0 and time_of_day < 18.0:
            return True
    return False

@Aggregator
def is_evening_pageview(event):
    if isinstance(event, TrackingDocument):
        time_of_day = epochfloat_to_time_of_day(es_utctime_str_to_epochfloat(event.created_at))
        if time_of_day >= 18.0:
            return True
    return False

@Aggregator
def is_night_pageview(event):
    if isinstance(event, TrackingDocument):
        time_of_day = epochfloat_to_time_of_day(es_utctime_str_to_epochfloat(event.created_at))
        if time_of_day < 6.0:
            return True
    return False

def split_off_versionstring(str):
    uasplit = str.split()
    try:
        str_firstpart, str_version = " ".join(uasplit[:-1]), uasplit[-1]
    except ValueError:
        str_firstpart, str_version = str, None
    return str_firstpart, str_version


class Session(EasyStr):
    """
    Contains the data of a session.

    Depending on the setup there may be one or more sessions per user.
    """

    def __init__(self, raw_event_stream, save_session_stream=True):
        """
        Constructor for Session

        Takes a list of events as its first arguments and calculates aggregate data based thereon
        """

        self._aggregated_properties = list()


        self.usertoken = raw_event_stream[0].usertoken or raw_event_stream[0].data['usertoken']

        first_trackingDocument, message = self._get_first_trackingDocument(raw_event_stream)
        assert message == "ok"


        self.control = False if first_trackingDocument.data['group'] in ('conversify', 'caas') else True
        raw_device_os, raw_user_agent =  httpagentparser.simple_detect(first_trackingDocument.data['userAgent'])
        self.device_os, self.device_os_version = split_off_versionstring(raw_device_os)
        self.user_agent, self.user_agent_version = split_off_versionstring(raw_user_agent)

        r = Referer(first_trackingDocument.data['referrer'])
        self.referer_info             = r # (all referer data)
        self.referer_known            = r.known   # is the referer known
        self.referer                  = r.referer # e.g. google
        self.referer_medium           = r.medium  # e.g. a (google) search
        self.referer_search_parameter = r.search_parameter
        self.referer_search_term      = r.search_term
        self.referer_uri_netloc       = r.uri.netloc
        self.referer_uri_path         = r.uri.path

        self.start_time =     raw_event_stream[0].created_at
        self.start_time_s =   es_utctime_str_to_epochfloat(self.start_time)
        self.end_time =       raw_event_stream[-1].created_at
        self.end_time_s =     es_utctime_str_to_epochfloat(self.end_time)
        self.converted =      isinstance(raw_event_stream[-1], ConversionDocument)
        self.raw_event_stream = raw_event_stream if save_session_stream else list()

        # calculate frequencies
        for aggregator, property_name in [
                (is_event,              'n_events'),
                (is_conversion,         'n_conversions'),
                (is_tracking,           'n_trackings'),
                (is_messageTracking,    'n_messageTrackings'),
                (is_socialMessage,      'n_socialMessages'),
                (is_scarcityMessage,    'n_scarcityMessages'),
                (is_otherMessage,       'n_otherMessages'),
                (is_morning_pageview,   'n_morning_pageviews'),
                (is_afternoon_pageview, 'n_afternoon_pageviews'),
                (is_evening_pageview,   'n_evening_pageviews'),
                (is_night_pageview,     'n_night_pageviews'),
        ]:
            self._add_aggregated_property(property_name, aggregator.count, raw_event_stream)

        # calculate boolean values
        for aggregator, property_name in [
                (is_conversion,      'any_conversions'),
                (is_messageTracking, 'any_messageTrackings'),
                (is_socialMessage,   'any_socialMessages'),
                (is_scarcityMessage, 'any_scarcityMessages'),
                (is_otherMessage,    'any_otherMessages')
        ]:
            self._add_aggregated_property(property_name, aggregator.any, raw_event_stream)

        # calculate sums
        for aggregator, property_name in [
            (amount_spent, 'total_spent'),
        ]:
            self._add_aggregated_property(property_name, aggregator.sum, raw_event_stream)

        # times at which a message was last seen
        for aggregator, property_name in [
            (messageTime        , 'last_messageTracking'),
            (socialMessageTime  , 'last_socialMessage'),
            (scarcityMessageTime, 'last_scarcityMessage'),
            (otherMessageTime   , 'last_otherMessage'),
        ]:
            self._add_aggregated_property(property_name, aggregator.last_not_none, raw_event_stream)

        # last event
        for aggregator, property_name in [
            (created_atTime, 'lastEvent'),
        ]:
            self._add_aggregated_property(property_name, aggregator.last, raw_event_stream)
        
        
        



    def _get_first_trackingDocument(self, raw_event_stream):
        for event in raw_event_stream:
            if isinstance(event, TrackingDocument):
                return event, "ok"
        for event in raw_event_stream:
            if isinstance(event, ConversionDocument):
                return event, "No first TrackingDocument returning first ConversionDocument instead"
        for event in raw_event_stream:
            if isinstance(event, MessageTrackingDocument):
                return event, "No first TrackingDocument returning first MessageTrackingDocument instead"


    def _add_property(self, property_name, property_value):
        setattr(self, property_name, property_value)
        self._properties.append(property_name)

    def _add_aggregated_property(self, property_name, aggregator, raw_event_stream):
        setattr(self, property_name, aggregator(raw_event_stream))
        self._aggregated_properties.append(property_name)
