#!/usr/bin/env python
from collections import OrderedDict
from pickle import load, dump
from sklearn import lda, svm

from pylabrecorder import *

from extract_data import ExtractData
from time_helper import *
from argument_helper import parse_arguments
from helper_methods import SiteInfo
from numpy.random import choice as resample

from numpy.random import random_integers


websites = list()
parse_arguments(globals(), sys.argv[1:])

extractData = ExtractData(websites=websites) if websites else ExtractData()
n_users = len(extractData.get_usertokens())



site_identifier = ("_".join([str(site) for site in websites]) if websites else 'all')
sitename, siteinfo = None, None
if len(websites) == 1:
    siteinfo = SiteInfo(websites[0])
    sitename = siteinfo.name
    site_identifier = sitename.replace(' ', '-')

def normalize(v):
    v = v - average(v)
    sigma=sqrt(var(v))
    if sigma==0:
        return v
    return v/sigma

class ArrayData(object):
    def __init__(self, data):
        self.n_rows = len(data)
        raw_arrays = OrderedDict()
        arrays = OrderedDict()
        for row in data:
            for key, value in row.items():
                l = raw_arrays.get(key, list())
                l.append(value)
                raw_arrays[key] = l
        for key, value in raw_arrays.items():
            arrays[key] = array(value)
        self.arrays = arrays

    def table(self):
        #TODO: these two lines may be removed later
        if not hasattr(self, "n_rows"):
            self.n_rows = len(self.arrays.values()[0])
        keys = [
#            "any_conversions",
            "any_messageTrackings",
            "any_otherMessages",
            "any_scarcityMessages",
            "any_socialMessages",

            "recent_messageTrackingBeforeCO",
            "recent_otherMessagesBeforeCO",
            "recent_scarcityMessagesBeforeCO",
            "recent_socialMessagesBeforeCO",

            "n_afternoon_pageviews",
            "n_conversions",
            "n_evening_pageviews",
            "n_events",
            "n_messageTrackings",
            "n_otherMessages",
            "n_scarcityMessages",
            "n_socialMessages",
            "n_trackings",
            "n_morning_pageviews",
            "n_night_pageviews",

            "avg_conversions",
            "avg_evening_pageviews",
            "avg_morning_pageviews",
            "avg_night_pageviews",
            "avg_messageTrackings",
            "avg_otherMessages",
            "avg_scarcityMessages",
            "avg_socialMessages",
            "avg_trackings",
            "avg_total_spent",

            "referer_known",
            "referer_medium",
            "referer_uri_netloc",
            "converted",
            "device_os",
            "user_agent",

            "time_period_s",
            "total_spent",
            "control",

        ]
        values = [array([site_identifier]*self.n_rows)] + [self.arrays[key] for key in keys]
        return ["website"]+keys, concatenate([reshape(value, (len(value), 1)) for value in values], 1)



def recent(X, Y):
    if Y is None:
        return 0
    else:
        return float(es_utctime_str_to_epochfloat(X) - es_utctime_str_to_epochfloat(Y))


filename = 'R_analysis_pkl/R_analysis_{}.pkl'.format(site_identifier)
try:
    array_data = load(open(filename))
except IOError:
    data = list()
    try:
        for i, user in enumerate(extractData.get_users(session_split_criterium="checkout")):
            print "****Loading, {:.2%} of total done...".format(i*1.0/n_users)
            assert len(user.sessions) >= 1
            session = user.sessions[0]
            row = OrderedDict()


            row["any_conversions"]       = session.any_conversions
            row["any_messageTrackings"]  = session.any_messageTrackings
            row["any_otherMessages"]     = session.any_otherMessages
            row["any_scarcityMessages"]  = session.any_scarcityMessages
            row["any_socialMessages"]    = session.any_socialMessages
                
            row["recent_messageTrackingBeforeCO"]  = recent(session.lastEvent, session.last_messageTracking)
            row["recent_otherMessagesBeforeCO"]    = recent(session.lastEvent, session.last_otherMessage   )
            row["recent_scarcityMessagesBeforeCO"] = recent(session.lastEvent, session.last_scarcityMessage)
            row["recent_socialMessagesBeforeCO"]   = recent(session.lastEvent, session.last_socialMessage  )

            row["n_afternoon_pageviews"] = session.n_afternoon_pageviews
            row["n_conversions"]         = session.n_conversions
            row["n_evening_pageviews"]   = session.n_evening_pageviews
            row["n_events"]              = session.n_events
            row["n_messageTrackings"]    = session.n_messageTrackings
            row["n_morning_pageviews"]   = session.n_morning_pageviews
            row["n_night_pageviews"]     = session.n_night_pageviews
            row["n_otherMessages"]       = session.n_otherMessages
            row["n_scarcityMessages"]    = session.n_scarcityMessages
            row["n_socialMessages"]      = session.n_socialMessages
            row["n_trackings"]           = session.n_trackings

            row["avg_conversions"]         = session.n_conversions         * 1.0 / session.n_events
            row["avg_evening_pageviews"]   = session.n_evening_pageviews   * 1.0 / session.n_events
            row["avg_messageTrackings"]    = session.n_messageTrackings    * 1.0 / session.n_events
            row["avg_morning_pageviews"]   = session.n_morning_pageviews   * 1.0 / session.n_events
            row["avg_afternoon_pageviews"] = session.n_afternoon_pageviews * 1.0 / session.n_events
            row["avg_night_pageviews"]     = session.n_night_pageviews     * 1.0 / session.n_events
            row["avg_otherMessages"]       = session.n_otherMessages       * 1.0 / session.n_events
            row["avg_scarcityMessages"]    = session.n_scarcityMessages    * 1.0 / session.n_events
            row["avg_socialMessages"]      = session.n_socialMessages      * 1.0 / session.n_events
            row["avg_trackings"]           = session.n_trackings           * 1.0 / session.n_events
            row["avg_total_spent"]         = session.total_spent           * 1.0 / session.n_events

            row["referer_known"]         = session.referer_known
            row["referer_medium"]        = session.referer_medium
            row["referer_uri_netloc"]    = session.referer_uri_netloc
            row["converted"]             = session.converted
            row["device_os"]             = session.device_os
            row["user_agent"]            = session.user_agent
            row["control"]               = session.control

            row["time_period_s"]         = float(es_utctime_str_to_epochfloat(session.end_time) - 
                                                 es_utctime_str_to_epochfloat(session.start_time))
            row["total_spent"]           = session.total_spent

            data.append(row)
    except KeyboardInterrupt:
        pass

    array_data = ArrayData(data)
    dump(array_data, open(filename, 'wb'))

R_datafile = open("R_analysis/data_{}.txt".format(site_identifier), 'w')
header, rows = array_data.table()
lines = list()
lines.append(" ".join(header)+"\n")
for row in rows:
    lines.append(" ".join(str(e).replace(" ", "_") for e in row)+"\n")
R_datafile.writelines(lines)

