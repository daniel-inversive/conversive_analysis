def average(self, iterable):
    """Calculates the average. If None is returned by the function then the element is ignored"""
    average = 0.0
    count = 0
    for i in iterable:
        e = function(i)
        if e is None:
            continue
        average += e
        count += 1
    return average/count

def sigma(self, iterable):
    """Calculates the square root of the variance. If None is returned by the function then the element is ignored"""
    element_list = list()
    for i in iterable:
        e = function(i)
        if e is None:
            continue
        element_list.append(e)
    if len(element_list) < 2:
        return None
    a = array(element_list)
    return sqrt(sum((a - average(a))**2)/(len(a)-1))

@Aggregator
def time_of_day(event):
    return epochfloat_to_time_of_day(t)

@Aggregator
def day_of_week(event):
    return epochfloat_to_day_of_week(t)

