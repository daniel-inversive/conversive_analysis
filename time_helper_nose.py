from time_helper import *
import settings

settings.local_timezone = 'Europe/Amsterdam'

def test_time_consistency():
    results = list()
    es_time_str = "2014-07-23T07:07:23.1415001"
    epoch_t = es_utctime_str_to_epochfloat(es_time_str)
    results.append("An elasticsearch timestamp: {} is represented in UTC".format(es_time_str))
    results.append("This timestamp can be converted to seconds since epoch: {}".format(repr(epoch_t)))

    local_ttuple = epochfloat_to_local_time_tuple(epoch_t)
    local_tstring = epochfloat_to_local_time_str(epoch_t)
    results.append("")
    results.append("The local time tuple based on seconds since epoch is: {}".format(local_ttuple))
    results.append("The corresponding time in human readable format is: {}".format(local_tstring))

    utc_ttuple = epochfloat_to_utc_time_tuple(epoch_t)
    utc_tstring = epochfloat_to_utc_time_str(epoch_t)
    results.append("")
    results.append("The UTC time tuple based on seconds since epoch is: {}".format(utc_ttuple))
    results.append("The corresponding time in human readable format is: {}".format(utc_tstring))

    assert results == [
        'An elasticsearch timestamp: 2014-07-23T07:07:23.1415001 is represented in UTC',
        'This timestamp can be converted to seconds since epoch: 1406099243.1415001',
        '',
        'The local time tuple based on seconds since epoch is: time.struct_time(tm_year=2014, tm_mon=7, tm_mday=23, '
                'tm_hour=9, tm_min=7, tm_sec=23, tm_wday=2, tm_yday=204, tm_isdst=1)',
        'The corresponding time in human readable format is: Wed, 23 Jul 2014 09:07:23 Europe/Amsterdam',
        '',
        'The UTC time tuple based on seconds since epoch is: time.struct_time(tm_year=2014, tm_mon=7, tm_mday=23, '
                'tm_hour=7, tm_min=7, tm_sec=23, tm_wday=2, tm_yday=204, tm_isdst=0)',
        'The corresponding time in human readable format is: Wed, 23 Jul 2014 07:07:23 UTC'
    ]

