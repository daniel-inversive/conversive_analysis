# To be removed
# "n_afternoon_pageviews",
# "n_conversions",
# "n_evening_pageviews",
# "n_trackings",
# "n_morning_pageviews",
# "n_night_pageviews",
# "avg_conversions",
# "avg_evening_pageviews",
# "avg_morning_pageviews",
# "avg_night_pageviews",
# "avg_trackings",
# "avg_total_spent",
# "referer_known",
# "referer_medium",
# "referer_uri_netloc",
# "converted",
# "device_os",
# "user_agent",
# "time_period_s",

pckg = c("MASS", "plyr", "gtools", "geoR")

is.installed <- function(mypkg){
   is.element(mypkg, installed.packages()[,1])
}

for(i in 1:length(pckg)) {
   if (!is.installed(pckg[i])){
        install.packages(pckg[i])
    }
}

library(MASS)
library(plyr)
library(gtools)
library(geoR)

closeAllConnections()
rm(list=ls())

par(mfrow=c(2,2))

data <- read.csv("data_ILoveSpeelgoed.nl.txt", sep=" ")
min_evts = 10
data = data[data$n_events >= min_evts,]

plot.histograms <- function(column, column.normalized, name, normalization.formula) {
    fstr = capture.output(normalization.formula)
    fstr = gsub("function\\(x\\) \\{ ", "", fstr)
    fstr = gsub(" \\}", "", fstr)
    fstr = gsub("x", name, fstr)
    hist(column[,1], xlab=name, ylab="count", col="red", main="Not Normalized")
    hist(column.normalized[,1], xlab=fstr, ylab="count", col="green", main="Normalized")
}


data.normalized = data.frame(matrix(NA, nrow=nrow(data), ncol=0))


# normalization functions
log1 <- function(x) { log(x) }
divlog1 <- function(x) { log(1.0/x) }

# list of operations for normalization
normalization.list = list(
    list("recent_messageTrackingBeforeCO", log1),
    list("recent_scarcityMessagesBeforeCO", log1),
    list("recent_socialMessagesBeforeCO", log1),
    list("recent_otherMessagesBeforeCO", log1),
    list("n_morning_pageviews", log1),
    list("n_afternoon_pageviews", log1),
    list("n_evening_pageviews", log1),
    list("n_events", log1),
    list("n_messageTrackings", log1),
    list("n_scarcityMessages", log1),
    list("n_socialMessages", log1),
    list("n_otherMessages", log1),
    list("avg_morning_pageviews", divlog1),
    list("avg_conversions", divlog1),
#    list("avg_afternoon_pageviews", log1),
    list("avg_evening_pageviews", divlog1),
    list("avg_messageTrackings", divlog1),
    list("avg_scarcityMessages", divlog1),
    list("avg_socialMessages", divlog1),
    list("avg_otherMessages", divlog1),
    list("avg_total_spent", divlog1),
    list("total_spent", log1)
)

for (idx in 1:length(normalization.list)) {
    x = normalization.list[[idx]]
    colname = toString(x[[1]])
    normalization.function = x[[2]]
    ncol = scale(data[colname])
    ncol = ncol - min(ncol)
    data.normalized[,colname] = normalization.function(ncol)[,1]
    plot.histograms(data[colname], data.normalized[colname], colname, normalization.function)
}

data.normalized[is.infinite(as.matrix(data.normalized))] <- NA
data.normalized[is.nan(as.matrix(data.normalized))] <- NA
data.normalized = as.data.frame(apply(data.normalized, 2, scale))

aov.data = data.normalized
aov.data$label = factor(data$control, levels=c("True", "False"), labels=c("control", "conversified"))

cat("******** ANCOVA DATA ********\n")
cat(str(aov.data))


#normalize <- function(x) {
#    return ((x - min(x)) / (max(x) - min(x)))
#}
#
#
## Load the data
#data <- data[sample(nrow(data)),] # Randomize the rows
#
## Normalize the columns
#data <- as.data.frame(lapply(data, normalize))
#
## Add correct label names and rename the "control" column
#data = rename(data, c("control"="label"))
#data$label <- factor(data$label, levels=c(1, 0), labels=c("control", "conversified"))
#
## Split the data up into X and Y
#X <- data
#X$label <- NULL
#Y <- data["label"]
#
## Add small random values to X to avoid 'too many ties' in knn
## X = X + data.frame(matrix(rnorm(nrow(X)*ncol(X), 0, 1E-6), nrow(X), ncol(X)))
#
#
## Some output to see what the data look like
#print(str(X))
#print(str(Y))
#print(round(prop.table(table(Y$label)) + 100, digits = 1))
#
## Split the data into test and train sets
#ntrain <- (nrow(data)*4) %/% 5
#X_train <- X[1:ntrain,]
#Y_train <- Y[1:ntrain,]
#X_test  <- X[(ntrain+1):nrow(X),]
#Y_test  <- Y[(ntrain+1):nrow(Y),]
#
## Some output to see what numbers of data we are dealing with
#cat("Number of rows in the train set:", nrow(X_train), "\n")
#cat("Number of rows in the test set:", nrow(X_test), "\n")
#

#        hist_data <- hist(column[,1]^2, xlab=colname, plot=FALSE)
#        plot(hist_data$mids, hist_data$count,
#            xlab=colname, ylab="count", title=message,
#            log="y", type='h', lwd=25, lend=2, col="grey")


