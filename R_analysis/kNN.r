pckg = c("class", "plyr")

is.installed <- function(mypkg){
   is.element(mypkg, installed.packages()[,1])
}

for(i in 1:length(pckg)) {
   if (!is.installed(pckg[i])){
        install.packages(pckg[i])
    }
}

library(class)
library(plyr)

closeAllConnections()
rm(list=ls())


data <- read.csv("data.txt", sep=" ")
data = data[data$n_events > 3,]

normalize <- function(x) {
    return ((x - min(x)) / (max(x) - min(x)))
}


# Load the data
data <- data[sample(nrow(data)),] # Randomize the rows

# Normalize the columns
data <- as.data.frame(lapply(data, normalize))

# Add correct label names and rename the "control" column
data = rename(data, c("control"="label"))
data$label <- factor(data$label, levels=c(1, 0), labels=c("control", "conversified"))

# Split the data up into X and Y
X <- data
X$label <- NULL
Y <- data["label"]

# Add small random values to X to avoid 'too many ties' in knn
# X = X + data.frame(matrix(rnorm(nrow(X)*ncol(X), 0, 1E-6), nrow(X), ncol(X)))


# Some output to see what the data look like
print(str(X))
print(str(Y))
print(round(prop.table(table(Y$label)) * 100, digits = 1))

# Split the data into test and train sets
ntrain <- (nrow(data)*4) %/% 5
X_train <- X[1:ntrain,]
Y_train <- Y[1:ntrain,]
X_test  <- X[(ntrain+1):nrow(X),]
Y_test  <- Y[(ntrain+1):nrow(Y),]

# Some output to see what numbers of data we are dealing with
cat("Number of rows in the train set:", nrow(X_train), "\n")
cat("Number of rows in the test set:", nrow(X_test), "\n")
k = 2*ceiling(sqrt(nrow(X_train)))
cat("Traning the set of", nrow(X_train), "samples with 2*sqrt(", nrow(X_train), ") = ", k, "nearest neighbors.\n")
p = knn(X_train, X_test, Y_train, k)
result = round(prop.table(table(p==Y_test))*100, digits=1)
print(result)
