#!/usr/bin/env python

def parse_arguments(globals_dict, arguments):
    for key, value in [a.split("=") for a in arguments if "=" in a]:
        value = eval(value)
        globals_dict[key] = value
