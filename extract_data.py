from shared.es.dao.conversiondao import ConversionDAO
from shared.es.dao.trackingdao import TrackingDAO
from shared.es.dao.messagetrackingdao import MessageTrackingDAO
from shared.es.model.conversiondocument import ConversionDocument
import sys

from session import Session
from user import User

class ExtractData(object):
    def __init__(self, max_size=10000000, websites=list()):
        self.max_size = max_size
        self.trackingDAO = TrackingDAO()
        self.conversionDAO = ConversionDAO()
        self.messageTrackingDAO = MessageTrackingDAO()
        self.websites = websites
        self.websites_query = {"website": "({0})".format(" OR ".join([str(s) for s in websites]))} if websites else dict()

    def get_usertokens(self):
        """Yields all unique usertokens"""
        usertokens = self.trackingDAO.distinct_values("data.usertoken", size=self.max_size, **self.websites_query)
        assert len(usertokens) != self.max_size
        if not hasattr(self, "_usertoken_cache"):
            self._usertoken_cache = [t['term'] for t in usertokens if t['term']]
        return self._usertoken_cache


    def get_safe_usertoken_abbreviation_length(self):
        usertokens = self.get_usertokens()
        n_usertokens = len(usertokens)
        for i in xrange(1, len(usertokens)):
            abbreviated_usertokens = [t[-i:] for t in usertokens]
            if len(set(abbreviated_usertokens)) == n_usertokens:
                break
        return i


    def get_time_sorted_data_stream_per_user(self, usertoken):
        """Yields the tracking-, conversion- and messageTracking- data for a specific user ordered by created_at timestamp"""
        sort = "created_at:asc"
        kwargs = {"data.usertoken": usertoken}
        kwargs.update(self.websites_query)
        trackings = self.trackingDAO.find(size=self.max_size, sort=sort, **kwargs)
        kwargs = {"usertoken": usertoken}
        kwargs.update(self.websites_query)
        conversions = self.conversionDAO.find(size=self.max_size, sort=sort, **kwargs)
        kwargs = {"usertoken": usertoken}
        kwargs.update(self.websites_query)
        messageTrackings = self.messageTrackingDAO.find(size=self.max_size, sort=sort, **kwargs)
        tracking, conversion, messageTracking = None, None, None
        while True:
            if not tracking:
                tracking = self._pop_first(trackings)
            if not conversion:
                conversion = self._pop_first(conversions)
            if not messageTracking:
                messageTracking = self._pop_first(messageTrackings)
            created_ats = [u.created_at for u in [tracking, conversion, messageTracking] if u is not None]
            if not created_ats:
                break
            min_created_at = min(created_ats)
            if tracking and tracking.created_at == min_created_at:
                yield tracking
                tracking = None
            elif conversion and conversion.created_at == min_created_at:
                yield conversion
                conversion = None
            elif messageTracking and messageTracking.created_at == min_created_at:
                yield messageTracking
                messageTracking = None

    def _get_time_sorted_session_streams(self, usertoken, split_criterium):
        event_stream = self.get_time_sorted_data_stream_per_user(usertoken)
        raw_event_stream = list()
        yielded_before = False
        while True:
            next_data = next(event_stream, None)
            if next_data:
                raw_event_stream.append(next_data)
            if \
            (
                    (next_data is None) or
                    ((split_criterium=="checkout") and
                            (raw_event_stream and (isinstance(raw_event_stream[-1], ConversionDocument))))
            ):
                if not raw_event_stream:
                    assert yielded_before
                else:
                    yield raw_event_stream
                    yielded_before = True
                raw_event_stream = list()
            if next_data is None:
                break

    def get_sessions(self, usertoken, save_session_stream=True, split_criterium=None):
        """split_criterium = "checkout", None"""
        sessions_found = False
        skipped_sessions = 0
        for i, raw_event_stream in enumerate(self._get_time_sorted_session_streams(usertoken, split_criterium)):
            try:
                yield Session(raw_event_stream, save_session_stream)
            except AssertionError, e:
                skipped_sessions += 1
                continue
            sessions_found = True
        if skipped_sessions:
            sys.stderr.write("Warning! Skipped some sessions: ({} session out of {}) for user {}.\n".format(skipped_sessions, i, usertoken))
            sys.stderr.flush()
        if not sessions_found:
            raise ValueError("No sessions found for user {0} ".format(usertoken))

    def get_users(self, session_split_criterium="checkout", save_session_stream=True):
        for usertoken in self.get_usertokens():
            try:
                yield(User(self.get_sessions(usertoken, save_session_stream=save_session_stream,
                                                        split_criterium=session_split_criterium)))
            except AssertionError, e:
                sys.stderr.write(str(e)+"\n")
                sys.stderr.write("Something went wrong with user {}.\n".format(usertoken))
                sys.stderr.flush()


    @staticmethod
    def _pop_first(es_result, default=None):
        try:
            return es_result.pop_first()
        except IndexError:
            return None

