#!/usr/bin/env python
import thread

enter_pressed = False

from extract_data import ExtractData
from pylabrecorder import *
from shared.es.model.trackingdocument import TrackingDocument
from time_helper import *

def input_thread(enter_pressed):
    while True:
        raw_input()
        enter_pressed[0] = True

enter_pressed = [False]
thread.start_new_thread(input_thread, (enter_pressed,))


p_start_t_s = es_utctime_str_to_epochfloat("2014-06-17T13:42:37.374530")
p_end_t_s = es_utctime_str_to_epochfloat("2014-07-24T00:00:00.0")

arguments = sys.argv[1:]
websites = list()
for arg in arguments:
    try:
        websites.append(int(arg))
    except ValueError:
        pass

figname = "/home/daniel/google_drive/histograms/histogram_sites_{}.pdf".format(websites)

extractData = ExtractData(websites=websites) if websites else ExtractData()
n_users = len(extractData.get_usertokens())

def plot_histogram(hist_list):
    f = recorded_figure(figsize=(14, 12))
    for i, range, bins, unit, xlabel in [
        (0, week,   35, day, "time [days]"),
        (1, day,    48, hour, "time [hours]"),
        (2, hour,   60, minute, "time [minutes]"),
        (3, minute, 60, second, "time [seconds]"),
    ]:
        ax = f.add_subplot(221+i)
        ax.hist(hist_list/unit, range=(0, range/unit), bins=bins, log=True)
        ax.set_xlabel(xlabel)
        ax.set_ylabel("count")
    f.suptitle("Time difference between pageviews. Sites: {}".format(websites))
    f.savefig(figname)
    show()

hist_list = array([], dtype=float128)
for i, user in enumerate(extractData.get_users(session_split_criterium=None)):
    print "**** {:.2%} of total done...".format(i*1.0/n_users)
    assert len(user.sessions) == 1
    timestamps = array([es_utctime_str_to_epochfloat(e.created_at) for e in user.sessions[0].raw_event_stream if isinstance(e, TrackingDocument)])
    time_diffs = timestamps[1:] - timestamps[:-1]
    hist_list = concatenate([hist_list, time_diffs])
    print "Number of samples: ", len(hist_list)
    print "Hit enter to plot an intermediate histogram..."
    if enter_pressed[0]:
        plot_histogram(hist_list)
        enter_pressed[0] = False

plot_histogram(hist_list)

