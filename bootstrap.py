#!/usr/bin/env python
from collections import namedtuple
from pickle import load, dump

from pylabrecorder import *

from extract_data import ExtractData
from time_helper import *
from argument_helper import parse_arguments
from helper_methods import SiteInfo

from numpy.random import random_integers



num_bootstrap = 100000
websites = list()

p_start_t_s = es_utctime_str_to_epochfloat("2014-06-17T13:42:37.374530")
p_end_t_s = es_utctime_str_to_epochfloat("2014-07-24T00:00:00.0")

parse_arguments(globals(), sys.argv[1:])

extractData = ExtractData(websites=websites) if websites else ExtractData()
n_users = len(extractData.get_usertokens())

user_conversion_fields = ['control', 'total_spent', 'any_conversions', 'n_conversions']
UserConversion = namedtuple('UserConversion', user_conversion_fields)
aggregate_data_fields = ['avg_turnover_per_visitor', 'avg_converted_visitors', 'avg_conversions']
aggregate_data_units = [(u'\u20ac', 0.01), ('%', 100.0), ('%', 100.0)]
AggregateData = namedtuple('AggregateData', aggregate_data_fields)

site_identifier = ("_".join([str(site) for site in websites]) if websites else 'all')
sitename, siteinfo = None, None
if len(websites) == 1:
    siteinfo = SiteInfo(websites[0])
    sitename = siteinfo.name
    site_identifier = sitename.replace(' ', '-')


resultfilename = "bootstrap_pkl/results.pkl"
filename = 'bootstrap_pkl/user_conversions_{}.pkl'.format(site_identifier)

if "showresults" in sys.argv:
    results = load(open(resultfilename))
    sorted_by_avg_turnover_per_visitor = list()
    order_and_key_list = list()
    for key, value in results.items():
        print "{}:".format(key)
        for key2, value2 in value.items():
            print "    {}:".format(key2)
            for key3, value3 in value2.items():
                print "        {}: {}".format(key3, value3)
        order = value['classic_AD']['change']
        order_and_key_list.append((order, key))
    sorted_keys = [key for order, key in sorted(order_and_key_list)][::-1]
    data_rows = list()
    for key in sorted_keys:
        row = (
            key,
            results[key]['avg_conversions']['change'],
            results[key]['avg_conversions']['p'],
            results[key]['avg_converted_visitors']['change'],
            results[key]['avg_converted_visitors']['p'],
            results[key]['avg_turnover_per_visitor']['change'],
            results[key]['avg_turnover_per_visitor']['p'],
            results[key]['classic_AD']['change'],
            results[key]['classic_AD']['p'],
        )
        if all(array(row[1:])) and results[key]['classic_AD']['conversions'] > 99:
            data_rows.append(row)
    (
        keys,
        avg_conversions_change,
        avg_conversions_p,
        avg_converted_visitors_change,
        avg_converted_visitors_p,
        avg_turnover_per_visitor_change,
        avg_turnover_per_visitor_p,
        classic_AD_change,
        classic_AD_p
    ) = [array(a) for a in zip(*data_rows)]

    yvalues = arange(len(keys))

    f = recorded_figure(figsize=(14, 7))
    fsp = f.subplotpars
    fsp.left = 0.14
    fsp.right = 0.96
    fsp.wspace = 0.04
    ax = f.add_subplot(121)
    xlims = -50, 50
    ylims = -1, len(yvalues)
    for yval in yvalues:
        for u in (-0.3, -0.1, 0.1, 0.3):
            ax.plot(xlims, [yval+u]*2, '-', color=(0.7, 0.7, 0.7, 1.0) if u != -0.3 else (1.0, 0.7, 0.7, 1.0))
    ax.plot([0, 0], ylims, '-k')
    ax.plot(classic_AD_change*100.0, yvalues+0.3, 'ob', label="Conv. Classic AD")
    ax.plot(avg_conversions_change*100, yvalues+0.1, "^b", label="Conv1. Bootstrap")
    ax.plot(avg_converted_visitors_change*100.0, yvalues-0.1, "xb", label="Conv2. Bootstrap")
    ax.plot(avg_turnover_per_visitor_change*100, yvalues-0.3, "xr", label="Turnover Bootstrap")
    ax.set_yticks(yvalues)
    ax.set_yticklabels(keys)
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)
    ax.set_xlabel("Change [%]")
    ax = f.add_subplot(122)
    xlims = 0, 1
    for yval in yvalues:
        for u in (-0.3, -0.1, 0.1, 0.3):
            ax.plot(xlims, [yval+u]*2, '-', color=(0.7, 0.7, 0.7, 1.0) if u != -0.3 else (1.0, 0.7, 0.7, 1.0))
    ax.plot([0.5, 0.5], ylims, '-k')
    ax.plot(classic_AD_p, yvalues+0.3, 'ob', label="Conv. Classic AD")
    ax.plot(avg_conversions_p, yvalues+0.1, "^b", label="Conv1. Bootstrap")
    ax.plot(avg_converted_visitors_p, yvalues-0.1, "xb", label="Conv2. Bootstrap")
    ax.plot(avg_turnover_per_visitor_p, yvalues-0.3, "xr", label="Turnover Bootstrap")
    ax.set_yticks(yvalues)
    ax.set_yticklabels([])
    ax.set_xlim(xlims)
    ax.set_ylim(ylims)
    ax.set_xlabel("p-Value")
    ax.legend(loc='best')

    f.savefig('/home/daniel/google_drive/bootstrap/bootstrap_validation.pdf')
    show()
    exit()


try:
    data = load(open(filename))
except IOError:
    data = list()
    try:
        for i, user in enumerate(extractData.get_users(session_split_criterium=None)):
            print "****Loading, {:.2%} of total done...".format(i*1.0/n_users)
            assert len(user.sessions) == 1
            s = user.sessions[0]
            data.append(UserConversion(
                control = s.control,
                total_spent = s.total_spent,
                any_conversions = s.any_conversions,
                n_conversions = s.n_conversions
            ))
            print data[-1]
    except KeyboardInterrupt:
        pass
    dump(data, open(filename, 'wb'))


def data_to_array_data(data):
    array_data_dict = dict()
    for name in user_conversion_fields:
        array_data_dict[name] = array([getattr(x, name) for x in data])
    return UserConversion(**array_data_dict)

def resample(array_data):
    m = len(array_data.control)
    resample = random_integers(0, m-1, m)
    array_data_dict = dict()
    for name in user_conversion_fields:
        array_data_dict[name] = getattr(array_data, name)[resample]
    return UserConversion(**array_data_dict)

def aggregate(array_data, control):
    select = array_data.control == control
    n_select = sum(select)
    return AggregateData(
        avg_turnover_per_visitor = sum(array_data.total_spent[select]*1.0/n_select),
        avg_converted_visitors = sum(array_data.any_conversions[select]*1.0/n_select),
        avg_conversions = sum(array_data.n_conversions[select]*1.0/n_select),
    )

array_data = data_to_array_data(data)
orig_control = aggregate(array_data, control=True)
orig_conversified = aggregate(array_data, control=False)
bootstrap_control = list()
bootstrap_conversified = list()
for i in xrange(num_bootstrap):
    if i % 100 == 0:
        print "****Bootstrapping, {:.2%} of total done...".format(i*1.0/num_bootstrap)
    bootstrap_control.append(aggregate(resample(array_data), control=True))
    bootstrap_conversified.append(aggregate(resample(array_data), control=False))

control_color = (0.5, 1.0, 0.5, 0.5)
conversified_color = (0.5, 0.5, 1.0, 0.5)
control_color2 = (0.0, 0.7, 0.0, 1.0)
conversified_color2 = (0.0, 0.0, 1.0, 1.0)

try:
    results = load(open(resultfilename))
except IOError:
    results = {}

site_results = results[site_identifier] = dict()
for i, (fieldname, units) in enumerate(zip(aggregate_data_fields, aggregate_data_units)):
    field_results = site_results[fieldname] = dict()
    f = figure()
    ax = f.add_subplot(111)
    control = array([getattr(x, fieldname) for x in bootstrap_control])*units[1]
    conversified = array([getattr(x, fieldname) for x in bootstrap_conversified])*units[1]
    nbins = 30
    ax.hist(control, color=control_color2, facecolor=control_color, normed=True, fill=True, bins=nbins, histtype='step', label='control')
    ax.hist(conversified, color=conversified_color2, facecolor=conversified_color, normed=True, fill=True, bins=nbins, histtype='step', label='conversified')
    p_conversified_gt_control = sum(conversified > control)*1.0/num_bootstrap
    if p_conversified_gt_control > 0.5:
        ax.set_title("Conversified > Control: confidence={:.1%}".format(p_conversified_gt_control),
                     color='g' if p_conversified_gt_control >= 0.95 else 'k')
    else:
        ax.set_title("Conversified < Control: confidence={:.1%}".format(1.0 - p_conversified_gt_control),
                     color='r' if p_conversified_gt_control <= 0.5 else 'k')
    ylim = ax.get_ylim()
    ax.plot([getattr(orig_control, fieldname)*units[1]]*2, ylim, '-', color=control_color2)
    ax.plot([getattr(orig_conversified, fieldname)*units[1]]*2, ylim, '-', color=conversified_color2)
    ax.set_xlabel(fieldname.replace('_', ' ').replace('avg', 'Average')+((" in "+units[0]) if units[0] else ""), size='large')
    ax.set_ylabel("Probability", size='large')
    ax.set_ylim(ylim)
    aa, bb = (control, conversified) if p_conversified_gt_control > 0.5 else (conversified, control)
    ax.set_xlim(average(aa)-4*sqrt(var(aa)), average(bb)+4*sqrt(var(bb)))
    field_results['p'] = p_conversified_gt_control
    field_results['control'] = getattr(orig_control, fieldname)
    field_results['conversified'] = getattr(orig_conversified, fieldname)
    change = (getattr(orig_conversified, fieldname) - getattr(orig_control, fieldname))/getattr(orig_control, fieldname)
    print fieldname, "change", change
    field_results['change'] = change
    # f.suptitle("(Orig. AD test for {} : change={:.2f}% conf.={:.2f}%)".format(sitename, siteinfo.change, siteinfo.confidence))
    fig_fname = '/home/daniel/google_drive/bootstrap/bootstrap_{}_{}.pdf'.format(fieldname, site_identifier)
    ax.legend()
    f.savefig(fig_fname)
if siteinfo:
    field_results = site_results['classic_AD'] = dict()
    p = siteinfo.confidence/100.0
    if siteinfo.change < 0:
        p = 1.0 - p
    field_results['p'] = p
    field_results['change'] = siteinfo.change/100.0
    field_results['conversions'] = siteinfo.conversions

dump(results, open(resultfilename, 'wb'))
print "Plotting", site_identifier
# show()