from extract_data import ExtractData

def test_extractData_sortedness():
    extractData = ExtractData()
    for i, token in enumerate(extractData.get_usertokens()):
        timestamps = [d.created_at for d in extractData.get_time_sorted_data_stream_per_user(token)]
        assert sorted(timestamps) == timestamps
        if i > 10:
            break