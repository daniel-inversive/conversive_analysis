import pytz
from numpy import float128
from time import strptime, strftime, localtime, gmtime
from calendar import timegm


from settings import local_timezone

__es_format_str =  "%Y-%m-%dT%H:%M:%S"
second = 1.0
minute = 60.0*second
hour = 60.0*minute
day = 24.0*hour
week = 7.0*day

local_tz = pytz.timezone(local_timezone)

def es_utctime_str_to_epochfloat(str):
    time_str, subseconds = str.split(".")
    subseconds = float128("0." + subseconds)
    time_tuple = strptime(time_str, __es_format_str)
    seconds_since_epoch = float128(timegm(time_tuple))
    return seconds_since_epoch + subseconds

def epochfloat_to_local_time_tuple(epoch_t):
    return localtime(epoch_t)

def epochfloat_to_local_time_str(epoch_t):
    return strftime("%a, %d %b %Y %H:%M:%S {}", epochfloat_to_local_time_tuple(epoch_t)).format(local_tz)

def epochfloat_to_utc_time_tuple(epoch_t):
    return gmtime(epoch_t)

def epochfloat_to_utc_time_str(epoch_t):
    return strftime("%a, %d %b %Y %H:%M:%S {}", epochfloat_to_utc_time_tuple(epoch_t)).format(pytz.utc)

def epochfloat_to_time_of_day(t):
    t_tuple = epochfloat_to_local_time_tuple(t)
    retval = (t_tuple[3]*1.0 # hours
           +  t_tuple[4]*1.0/(60) # minute
           +  t_tuple[5]*1.0/(60*60)) # second
    return retval


def epochfloat_to_day_of_week(t):
    t_tuple = epochfloat_to_local_time_tuple(t)
    retval = (t_tuple[3]*1.0/24 # hours
              +  t_tuple[4]*1.0/(24*60) # minute
              +  t_tuple[5]*1.0/(24*60*60) # second
              +  t_tuple[6]*1.0)# day of week
    return retval


__all__ = ['epochfloat_to_local_time_str', 'epochfloat_to_local_time_tuple', 'epochfloat_to_utc_time_str',
           'epochfloat_to_utc_time_tuple', 'es_utctime_str_to_epochfloat', 'gmtime', 'week', 'day', 'hour', 'minute',
           'second']
