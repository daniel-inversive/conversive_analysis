#!/usr/bin/env python

from pylab import *
from scipy import random

a = sorted(random.normal(1,0.2,10000))
# b = sorted(random.uniform(0.9,1.1,10000))
c = sorted(random.normal(1,0.2,10000)**2)

f = figure()
ax = f.add_subplot(111)
ax.plot(concatenate([arange(0,1, 1.0/len(a)),array([0])]), concatenate([a,array([0])]), label='a')
# ax.plot(concatenate([arange(0,1, 1.0/len(b)),array([0])]), concatenate([b,array([0])]), label='b')
ax.plot(concatenate([arange(0,1, 1.0/len(c)),array([0])]), concatenate([c,array([0])]), label='c')

show()


