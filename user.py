#!/usr/bin/env python
import numbers
import sys

from easy_str import EasyStr

class User(EasyStr):
    """Contains the sessions of a specific user plus aggregated data"""

    def __init__(self, sessions):
        """Constructor for User"""
        sessions = list(sessions)
        self.sessions = sessions
        self.n_sessions = len(sessions)
        self.start_time = sessions[0].start_time
        self.end_time = sessions[-1].end_time
        self.start_time_s = sessions[0].start_time_s
        self.end_time_s = sessions[-1].end_time_s

        for key, value in self.sessions[0].__dict__.items():
            if isinstance(value, numbers.Number):
                setattr(self, key+"_total", 0.0)
                setattr(self, key+"_avg", 0.0)
            if isinstance(value, bool):
                setattr(self, key+"_any", False)
                setattr(self, key+"_all", True)
        for session in sessions:
            for key, value in session.__dict__.items():
                if isinstance(value, numbers.Number):
                    setattr(self, key+"_total", getattr(self, key+"_total") + getattr(session, key))
                    setattr(self, key+"_avg", getattr(self, key+"_avg") + getattr(session, key)*1.0/self.n_sessions)
                if isinstance(value, bool):
                    setattr(self, key+"_any", getattr(self, key+"_any") or getattr(session, key))
                    setattr(self, key+"_all", getattr(self, key+"_all") and getattr(session, key))

