from numpy import *
from easy_str import EasyStr

class Bootstrapper(EasyStr):
    def __init__(self, samples, aggregation_function, n_bootstraps=10000, verbose=True):
        samples = array(samples)
        self.n_samples = len(samples)
        self.n_bootstraps = n_bootstraps
        sample_nums = arange(self.n_samples)

        self.orig_aggregate = aggregation_function(samples)

        aggregates = zeros(n_bootstraps)
        for i in xrange(n_bootstraps):
            resampled_nums = random.choice(sample_nums, self.n_samples) # resample with replacement
            resampled = samples[resampled_nums]
            aggregates[i] = aggregation_function(resampled)
            if i % 100 == 0:
                print "Bootstrapping: {:.1%}".format(i*1.0/n_bootstraps)

        self.mean = mean(aggregates)
        self.std = std(aggregates, ddof=1)
        self.calculated_percentiles = [0.1, 1.0, 5.0, 25.0, 50.0, 75.0, 95.0, 99.0, 99.9]
        self.p0p1, self.p1, self.p5, self.Q1, self.Q2, self.Q3, self.p95, self.p99, self.p99p9 = \
            self.percentiles = percentile(aggregates, self.calculated_percentiles)
        self.median = self.Q2
        self.bootstrapped_aggregates = aggregates

    def p_value_right(self, x):
        return sum(self.bootstrapped_aggregates > x)*1.0/self.n_bootstraps

    def p_value_left(self, x):
        return 1.0 - self.p_value_right(x)
