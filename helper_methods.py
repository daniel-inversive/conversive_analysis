class SiteInfo(object):
    __siteinfo = {
        2  : ("Oppassers.nl", "https://www.oppassers.nl/", 737, -14.59, 98.89,                      ),
        4  : ("Tafelreserveren.nl", "http://www.tafelreserveren.nl/", 231, -6.08, 68.13,            ),
        7  : ("Ledworld.nl", "http://ledworld.nl/", 462, -17.49, 97.98,                             ),
        13 : ("BadkamerXXL.nl", "http://www.badkamerxxl.nl/", 15, 22.24, 64.75,                     ),
        14 : ("iphone2day.nl", "http://www.iphone2day.nl/", 621, 4.37, 70.45,                       ),
        15 : ("iAccessoires.nl", "http://www.iaccessoires.nl/", 204, 1.36, 53.84,                   ),
        16 : ("Smartwatchkopen.nu", "http://smartwatchkopen.nu/", 26, -9.22, 59.56,                 ),
        17 : ("Artform", "http://www.artform.nl/", 0, 0.00, 0.00,                                   ),
        18 : ("Van Donzel", "http://www.vandonzel.nl/", 0, 0.00, 0.00,                              ),
        20 : ("Keukenmarkt", "http://www.keukenmarkt-nederland.nl/", 167, 19.44, 87.06,             ),
        6  : ("ILoveSpeelgoed.nl", "https://www.ilovespeelgoed.nl/", 1233, 5.92, 84.44,             ),
        11 : ("Computeroutlet.nl", "http://www.computeroutlet.nl/", 376, 8.56, 78.48,               ),
        9  : ("Comcol.nl", "http://comcol.nl/", 1180, 0.99, 56.79,                                  ),
        19 : ("Green lab", "http://www.green-lab.nl/", 5, 170.53, 84.56,                            ),
        21 : ("Kant en klaar hagen", "http://www.kantenklaarhagen.nl/", 13, 5.73, 53.93,            ),
        22 : ("Polyester plantenbakken", "http://www.polyesterplantenbakken.nl/", 34, -3.66, 54.27, ),
        24 : ("Houtenspeelhuis", "http://www.houtenspeelhuis.nl/", 2, -32.61, 60.67,                ),
        25 : ("Grote Tuintafels", "http://www.grotetuintafels.nl/", 1, 0.00, 84.16,                 ),
        26 : ("Tuinonderhoudshop", "http://www.tuinonderhoudshop.nl/", 0, 0.00, 0.00,               ),
        27 : ("Tuin-Online", "http://www.tuin-online.nl/", 18, -46.38, 89.67,                       ),
        23 : ("Design tuinhaarden", "http://www.designtuinhaarden.nl/", 29, 73.53, 92.29,           ),
    }

    def __init__(self, site_num):
        self.name, self.url, self.conversions, self.change, self.confidence = SiteInfo.__siteinfo[site_num]