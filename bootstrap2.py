#!/usr/bin/env python
from collections import OrderedDict
from pickle import load, dump
from sklearn import lda, svm

from pylabrecorder import *

from extract_data import ExtractData
from time_helper import *
from argument_helper import parse_arguments
from helper_methods import SiteInfo
from numpy.random import choice as resample
from bootstrapper import Bootstrapper

from numpy.random import random_integers


websites = list()
plot_results = False
parse_arguments(globals(), sys.argv[1:])

extractData = ExtractData(websites=websites) if websites else ExtractData()
n_users = len(extractData.get_usertokens())

class AggregateSessions(object):
    def __init__(self, filter_function, value_function, simple_aggregate_function):
        self._filter_function = filter_function
        self._value_function = value_function
        self._aggregate_function = simple_aggregate_function

    def __call__(self, users):
        control_values, conversified_values = list(), list()
        for user in users:
            for session in user.sessions:
                if filter_function(session):
                    if session.control:
                        control_values.append(self._value_function(session))
                    else:
                        conversified_values.append(self._value_function(session))
        return self._aggregate_function(conversified_values)/self._aggregate_function(control_values)


site_identifier = ("_".join([str(site) for site in websites]) if websites else 'all')
sitename, siteinfo = None, None
if len(websites) == 1:
    siteinfo = SiteInfo(websites[0])
    sitename = siteinfo.name
    site_identifier = sitename.replace(' ', '-')


if not plot_results:
    filename = 'bootstrap2_pkl/bootstrap2_{}.pkl'.format(site_identifier)
    try:
        data = load(open(filename))
    except IOError:
        data = list()
        try:
            for i, user in enumerate(extractData.get_users(session_split_criterium="checkout", save_session_stream=False)):
                if i % 200 == 0:
                    print "****Loading, {:.2%} of total done...".format(i*1.0/n_users)
                assert len(user.sessions) >= 1
                data.append(user)
        except KeyboardInterrupt:
            pass
        dump(data, open(filename, 'wb'))

result_filename = 'bootstrap2_pkl/results.pkl'.format(site_identifier)
try:
    results = load(open(result_filename))
except IOError:
    results = dict()

if not plot_results:
    results[site_identifier] = dict()
    value_function = lambda session : session.total_spent
    def simple_aggregate_function(values):
        return average(values)

    for filter_name, filter_function in (
        ("all",         lambda session : True),
        ("any_message", lambda session : session.any_messageTrackings),
        ("social",      lambda session : session.any_socialMessages),
        ("scarcity",    lambda session : session.any_scarcityMessages),
        ("other",       lambda session : session.any_otherMessages),
    ):
        print "Bootstrapping", site_identifier, "for", filter_name+"."
        aggregate_function = AggregateSessions(filter_function, value_function, simple_aggregate_function)
        bootstrapper = Bootstrapper(data, aggregate_function, n_bootstraps=1000)
        results[site_identifier][filter_name] = bootstrapper
    dump(results, open(result_filename, 'w'))


if plot_results:
    f = recorded_figure(figsize=(7, 10))
    f.subplotpars.left = 0.36
    ax = f.add_subplot(111)
    yticks = list()
    yticklabels = list()
    count = 0
    firstlegend = True
    for site_identifier_s in sorted(results.keys()):
        btest_ok = results[site_identifier_s]["any_message"]
        if btest_ok.n_samples < 100 or isnan(btest_ok.mean) or isinf(btest_ok.mean):
            continue
        count -= 1
        yticks.append(count)
        yticklabels.append("*** {} ***".format(site_identifier_s))
        for selection in ("all", "any_message", "social", "scarcity", "other"):
            bootstrap = results[site_identifier_s][selection]
            if bootstrap.n_samples < 10 or isnan(bootstrap.mean) or isinf(bootstrap.mean):
                continue
            count -= 1
            l_1_99   = ax.plot([bootstrap.p1, bootstrap.p99], [count]*2, '-|g', label="1%-99%")
            l_5_95   = ax.plot([bootstrap.p5, bootstrap.p95], [count]*2, '-|b', label="5%-95%")
            l_Q1Q3   = ax.plot([bootstrap.Q1, bootstrap.Q3], [count]*2,  '-|r', label="25%-75%")
            l_orig   = ax.plot([bootstrap.orig_aggregate], [count], 'og', label="orig")
            l_mean   = ax.plot([bootstrap.mean], [count], 'ob', label="mean")
            l_median = ax.plot([bootstrap.median], [count], 'or', label="median")
            print bootstrap
            yticks.append(count)
            yticklabels.append("{}: {:.1%}".format(selection, bootstrap.p_value_right(1.0)))
            if firstlegend:
                ax.legend()
                firstlegend = False
        count -= 1
    ax.set_yticks(yticks)
    ax.set_yticklabels(yticklabels)
    ylim = yticks[-1]-2, yticks[0]+2
    ax.plot([1.0]*2, ylim, '-k')
    ax.set_ylim()
    ax.set_xlabel("Increase in turnover per customer")
    show()



