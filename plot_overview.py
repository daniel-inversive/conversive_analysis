#!/usr/bin/env python
import sys

from extract_data import ExtractData
from pylabrecorder import *
from shared.es.model.trackingdocument import TrackingDocument
from shared.es.model.messagetrackingdocument import MessageTrackingDocument

from time_helper import es_utctime_str_to_epochfloat

figname = "/home/daniel/google_drive/overview/overview_sites_{}_{:03}.pdf"

p_start_t_s = es_utctime_str_to_epochfloat("2014-06-17T13:42:37.374530")
p_end_t_s = es_utctime_str_to_epochfloat("2014-07-24T00:00:00.0")

arguments = sys.argv[1:]
websites = list()
for arg in arguments:
    try:
        websites.append(int(arg))
    except ValueError:
        pass

day = 24.0*60*60

extractData = ExtractData(websites=websites) if websites else ExtractData()
n_users = len(extractData.get_usertokens())
n_users_per_plot = 100
l0a, l0b, l1a, l1b, l2, l3 = [None]*6
for i, user in enumerate(extractData.get_users()):
    imod = i % n_users_per_plot
    idiv = i // n_users_per_plot
    imodi = n_users_per_plot - imod
    print "**** Generating plot {}, {:.2%} done".format(idiv, imod*1.0/n_users_per_plot)
    print "**** {:.2%} of total done...".format(i*1.0/n_users)
    if 'print' in arguments:
        print user
        print
    if imod == 0 or i == n_users-1:
        labels = True
        if i != 0:
            ls, ss = list(), list()
            for (l, s) in  zip([l0a, l0b, l1a, l1b, l2, l3],
                               ['control', 'conversified', 'converted', 'not converted', 'pageview', 'message']):
                if l:
                    ls.append(l[0])
                    ss.append(s)
            f.legend(ls, ss)
            ax.set_xlim(-1, (p_end_t_s-p_start_t_s)/day+1)
            ax.set_ylim((idiv-1)*n_users_per_plot-1, (idiv)*n_users_per_plot + 10)
            if 'show' in arguments:
                show()
            f.savefig(figname.format("_".join([str(s) for s in websites]), idiv))
        f = recorded_figure(figsize=(8.267, 11.692))
        l0a, l0b, l1a, l1b, l2, l3 = [None]*6
        ax = f.add_subplot(111)
        ax.set_xlabel("time [days]")
        ax.set_ylabel("users")
        ax.set_title("Websites: {} plot {}/{}".format(websites, idiv+1, int(ceil(n_users * 1.0 / n_users_per_plot))))
    l0 = ax.plot([-1, (p_end_t_s - p_start_t_s)/day+1], [i, i], '-',
                 color="#AAAAAA" if user.control_any else "#88FF88")
    if user.control_any:
        l0a = l0
    else:
        l0b = l0
    for session in user.sessions:
        if 'print' in arguments:
            print session
        tracking_times = [es_utctime_str_to_epochfloat(e.created_at)
                          for e in session.raw_event_stream if isinstance(e, TrackingDocument)]
        message_tracking_times = [es_utctime_str_to_epochfloat(e.created_at)
                                  for e in session.raw_event_stream if isinstance(e, MessageTrackingDocument)]

        l1 = ax.plot(
            [(session.start_time_s - p_start_t_s)/day, (session.end_time_s - p_start_t_s)/day],
            [i, i],
            '-b' if session.converted else '-r')
        if session.converted:
            l1a = l1
        else:
            l1b = l1
        l2 = ax.plot(
            (array(tracking_times)-p_start_t_s)/day,
            [i]*len(tracking_times),
            '.k')
        l3 = ax.plot(
            (array(message_tracking_times)-p_start_t_s)/day,
            [i]*len(message_tracking_times),
            '.g')
