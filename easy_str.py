import numbers

class EasyStr(object):
    def __str__(self):
        lines = ["{0}:".format(type(self).__name__)]
        for key, value in self.__dict__.items():
            if isinstance(value, numbers.Number) or isinstance(value, bool) or isinstance(value, basestring):
                lines.append("    {0}: {1}".format(key, value))
            if hasattr(value, "__iter__"):
                lines.append("    {0}: {1}".format(key, str(value)[:50]))
        return "\n".join(lines)
